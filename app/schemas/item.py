from tortoise.contrib.pydantic import pydantic_model_creator

from app import models

Item = pydantic_model_creator(models.item.Item, name='Item')
ItemBase = pydantic_model_creator(models.item.Item, name='ItemBase', exclude_readonly=True)
ItemCreate = ItemUpdate = ItemBase

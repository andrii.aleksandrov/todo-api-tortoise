from tortoise.contrib.pydantic import pydantic_model_creator

from app import models

User = pydantic_model_creator(models.user.User, name='User')
UserCreate = pydantic_model_creator(models.user.User, name='UserCreate', exclude_readonly=True)

import asyncio

from app.models import User
from app.schemas import user as user_schemas
from app.crud import crud_user


def test_create_user(event_loop: asyncio.AbstractEventLoop):
    user_data = {
        'username': 'testcreate_user',
        'password': 'test_password'
    }
    user_in: user_schemas.UserCreate = user_schemas.UserCreate(**user_data)

    async def create_db_user() -> User:
        user: User = await crud_user.user.create(user_in)
        return user

    user_db = event_loop.run_until_complete(create_db_user())

    assert isinstance(user_db, User)
    assert user_db.username == user_in.username
    assert hasattr(user_db, 'password')
    assert hasattr(user_db, 'id')


def test_get_user(event_loop: asyncio.AbstractEventLoop):
    user_data = {
        'username': 'testget_user',
        'password': 'test_password'
    }
    user_in: user_schemas.UserCreate = user_schemas.UserCreate(**user_data)

    async def create_db_user() -> User:
        user: User = await crud_user.user.create(user_in)
        return user

    user_create_db = event_loop.run_until_complete(create_db_user())

    async def get_db_user() -> User:
        user: User = await crud_user.user.get(id=user_create_db.id)
        return user

    user_get_db = event_loop.run_until_complete(get_db_user())
    assert user_get_db.username == user_in.username
    assert user_get_db.password == user_create_db.password
    assert user_get_db.id == user_create_db.id


def test_get_user_by_username(event_loop: asyncio.AbstractEventLoop):
    user_data = {
        'username': 'testgetusername_user',
        'password': 'test_password'
    }
    user_in: user_schemas.UserCreate = user_schemas.UserCreate(**user_data)

    async def create_db_user() -> User:
        user: User = await crud_user.user.create(user_in)
        return user

    user_create_db = event_loop.run_until_complete(create_db_user())

    async def get_db_user() -> User:
        user: User = await crud_user.user.get_by_username(username=user_create_db.username)
        return user

    user_get_db = event_loop.run_until_complete(get_db_user())
    assert user_get_db.username == user_in.username
    assert user_get_db.password == user_create_db.password
    assert user_get_db.id == user_create_db.id

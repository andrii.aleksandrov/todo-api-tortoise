from typing import List
import asyncio
import pytest
from tortoise.exceptions import DoesNotExist

from app.models import User, Item
from app.schemas import user as user_schemas
from app.schemas import item as item_schemas
from app.crud import crud_user, crud_item


async def create_user(username) -> User:
    user_data = {
        'username': username,
        'password': 'test_password'
    }
    user_in: user_schemas.UserCreate = user_schemas.UserCreate(**user_data)

    user: User = await crud_user.user.create(user_in)
    return user


def test_create_item(event_loop: asyncio.AbstractEventLoop):
    owner_username = 'testitemowner'
    user_db = event_loop.run_until_complete(create_user(owner_username))

    item_data = {
        'title': 'mytitle',
        'description': 'my desc text',
    }
    item_in: item_schemas.ItemCreate = item_schemas.ItemCreate(**item_data)

    async def create_db_item() -> Item:
        item: Item = await crud_item.item.create(item=item_in, owner_id=user_db.id)
        return item

    item_db: Item = event_loop.run_until_complete(create_db_item())

    assert isinstance(item_db, Item)
    assert item_db.title == item_in.title
    assert item_db.description == item_in.description
    assert item_db.owner_id == user_db.id


def test_get_item(event_loop: asyncio.AbstractEventLoop):
    owner_username = 'testitemowner2'
    user_db = event_loop.run_until_complete(create_user(owner_username))

    item_data = {
        'title': 'mytitle',
        'description': 'my desc text',
    }
    item_in: item_schemas.ItemCreate = item_schemas.ItemCreate(**item_data)

    async def create_db_item() -> Item:
        item: Item = await crud_item.item.create(item=item_in, owner_id=user_db.id)
        return item

    item_create_db = event_loop.run_until_complete(create_db_item())

    async def get_db_item() -> Item:
        item: Item = await crud_item.item.get(item_id=item_create_db.id)
        return item

    item_get_db = event_loop.run_until_complete(get_db_item())

    assert item_get_db.title == item_in.title
    assert item_get_db.description == item_in.description
    assert item_get_db.owner_id == item_create_db.owner_id


def test_get_items(event_loop: asyncio.AbstractEventLoop):
    owner_username = 'testitemowner3'
    user_db = event_loop.run_until_complete(create_user(owner_username))

    item_data = {
        'title': 'mytitle',
        'description': 'my desc text',
    }
    item_in: item_schemas.ItemCreate = item_schemas.ItemCreate(**item_data)

    async def create_db_item() -> Item:
        item: Item = await crud_item.item.create(item=item_in, owner_id=user_db.id)
        return item

    async def get_db_items() -> List[Item]:
        items: List[Item] = await crud_item.item.get_collection(owner_id=user_db.id)
        return items

    async def count_db_items(items):
        count = await items.count()
        return count

    async def get_from_queryset(items_db):
        items = await item_schemas.Item.from_queryset(items_db)
        return items

    event_loop.run_until_complete(create_db_item())

    items_get_db = event_loop.run_until_complete(get_db_items())
    items_count = event_loop.run_until_complete(count_db_items(items_get_db))
    assert items_count == 1

    event_loop.run_until_complete(create_db_item())

    items_get_db = event_loop.run_until_complete(get_db_items())
    items_count = event_loop.run_until_complete(count_db_items(items_get_db))
    assert items_count == 2

    items = event_loop.run_until_complete(get_from_queryset(items_get_db))

    for item in items:
        assert item.title == item_in.title
        assert item.description == item_in.description


def test_update_item(event_loop: asyncio.AbstractEventLoop):
    owner_username = 'testitemupdateowner'
    user_db = event_loop.run_until_complete(create_user(owner_username))

    item_old_data = {
        'title': 'old title',
        'description': 'old description text',
    }

    item_new_data = {
        'title': 'new title',
        'description': 'new description text',
    }

    item_in: item_schemas.ItemCreate = item_schemas.ItemCreate(**item_old_data)
    item_update: item_schemas.ItemUpdate = item_schemas.ItemUpdate(**item_new_data)

    async def create_db_item() -> Item:
        item: Item = await crud_item.item.create(item=item_in, owner_id=user_db.id)
        return item

    item_db: Item = event_loop.run_until_complete(create_db_item())

    assert item_db.title == item_in.title
    assert item_db.description == item_in.description

    old_item_id = item_db.id

    async def update_db_item():
        await crud_item.item.update(item_id=old_item_id, item_update=item_update)

    event_loop.run_until_complete(update_db_item())

    async def get_db_item() -> Item:
        item: Item = await crud_item.item.get(item_id=old_item_id)
        return item

    item_db = event_loop.run_until_complete(get_db_item())

    assert item_db.title == item_update.title
    assert item_db.description == item_update.description
    assert item_db.id == old_item_id


def test_delete_item(event_loop: asyncio.AbstractEventLoop):
    owner_username = 'testitemdeleteowner'
    user_db = event_loop.run_until_complete(create_user(owner_username))

    item_data = {
        'title': 'old title',
        'description': 'old description text',
    }

    item_in: item_schemas.ItemCreate = item_schemas.ItemCreate(**item_data)

    async def create_db_item() -> Item:
        item: Item = await crud_item.item.create(item=item_in, owner_id=user_db.id)
        return item

    item_db: Item = event_loop.run_until_complete(create_db_item())

    async def delete_db_item():
        await crud_item.item.delete(item_id=item_db.id)

    event_loop.run_until_complete(delete_db_item())

    async def get_db_item() -> Item:
        item: Item = await crud_item.item.get(item_id=item_db.id)
        return item

    with pytest.raises(DoesNotExist):
        item_db = event_loop.run_until_complete(get_db_item())

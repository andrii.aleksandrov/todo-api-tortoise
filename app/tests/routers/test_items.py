from fastapi.testclient import TestClient

from app.routers.items import crud_item
from app.models import Item

item_data = {
    'id': 102,
    'title': 'my get item title',
    'description': 'my get desc text',
}


def test_get_item(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id']
    item: Item = Item(**item_data)

    async def mocked_get_item(item_id):
        return item

    monkeypatch.setattr(crud_item.item, 'get', mocked_get_item)
    response = client.get('items/1', headers=user_info['token_headers'])

    assert response.status_code == 200
    assert item.title == response.json()['title']
    assert item.description == response.json()['description']


def test_get_item_not_owner(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id'] + 1
    item: Item = Item(**item_data)

    async def mocked_get_item(item_id):
        return item

    monkeypatch.setattr(crud_item.item, 'get', mocked_get_item)
    response = client.get('items/1', headers=user_info['token_headers'])
    assert response.status_code == 403


def test_create_item(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id']
    item_in: Item = Item(**item_data)

    async def mocked_create_item(item, owner_id):
        return item_in

    monkeypatch.setattr(crud_item.item, 'create', mocked_create_item)
    item_create = {'title': item_data['title'], 'description': item_data['description']}
    response = client.post('items/', json=item_create, headers=user_info['token_headers'])

    assert response.status_code == 201
    assert item_in.title == response.json()['title']
    assert item_in.description == response.json()['description']


def test_update_item(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id']
    item_in: Item = Item(**item_data)

    async def mocked_update_item(*args, **kwargs):
        return item_in

    monkeypatch.setattr(crud_item.item, 'update', mocked_update_item)
    monkeypatch.setattr(crud_item.item, 'get', mocked_update_item)
    item_update = {'title': item_data['title'], 'description': item_data['description']}
    response = client.put('items/102', json=item_update,
                          headers=user_info['token_headers'])

    assert response.status_code == 200
    assert item_in.title == response.json()['title']
    assert item_in.description == response.json()['description']


def test_update_item_not_found(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id']

    async def mocked_get_item(item_id):
        return None

    monkeypatch.setattr(crud_item.item, 'get', mocked_get_item)
    item_update = {'title': item_data['title'], 'description': item_data['description']}
    response = client.put('items/102', json=item_update,
                          headers=user_info['token_headers'])
    assert response.status_code == 404


def test_update_item_not_owner(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id'] + 1
    item_in: Item = Item(**item_data)

    async def mocked_get_item(item_id):
        return item_in

    monkeypatch.setattr(crud_item.item, 'get', mocked_get_item)
    item_update = {'title': item_data['title'], 'description': item_data['description']}
    response = client.put('items/102', json=item_update,
                          headers=user_info['token_headers'])
    assert response.status_code == 403


def test_delete_item(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id']
    item_in: Item = Item(**item_data)

    async def mocked_get_item(item_id):
        return item_in

    async def mocked_delete_item(item_id):
        return

    monkeypatch.setattr(crud_item.item, 'get', mocked_get_item)
    monkeypatch.setattr(crud_item.item, 'delete', mocked_delete_item)
    response = client.delete('items/102', headers=user_info['token_headers'])
    assert not response.text


def test_delete_not_owner(client: TestClient, monkeypatch, user_info):
    item_data['owner_id'] = user_info['user_id'] + 1
    item_in: Item = Item(**item_data)

    async def mocked_get_item(item_id):
        return item_in

    monkeypatch.setattr(crud_item.item, 'get', mocked_get_item)
    response = client.delete('items/102', headers=user_info['token_headers'])
    assert response.status_code == 403

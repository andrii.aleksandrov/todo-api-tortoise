from typing import Generator
from tortoise.contrib.test import finalizer, initializer
import asyncio

import pytest
from fastapi.testclient import TestClient
from tortoise.contrib.fastapi import register_tortoise
import fastapi_bearer_auth as fba

from app.crud import crud_user
from app.schemas import user as schemas_user
from app.main import app

register_tortoise(
    app,
    db_url='sqlite://:memory:',
    modules={"models": ["app.models"]},
    generate_schemas=True,
    add_exception_handlers=True,
)


@pytest.fixture(scope="session")
def client() -> Generator:
    initializer(['app.models'])
    with TestClient(app) as c:
        yield c
    finalizer()


@pytest.fixture(scope='module')
def event_loop(client: TestClient) -> Generator:
    yield client.task.get_loop()


def get_authentication_token(client: TestClient, event_loop: asyncio.AbstractEventLoop, username: str):
    """
    Return a valid token for the user with given username.

    If the user doesn't exist it is created first.
    """
    raw_password = 'testpassword'

    async def get_user_by_username():
        user = await crud_user.user.get_by_username(username=username)
        return user

    async def create_user(user_in_create):
        user = await crud_user.user.create(user=user_in_create)
        return user

    async def get_hashed_password(password):
        password = await fba.call_config('get_password_hash', password)
        return password

    user = event_loop.run_until_complete(get_user_by_username())
    password = event_loop.run_until_complete(get_hashed_password(raw_password))
    if not user:
        user_in_create = schemas_user.UserCreate(username=username, password=password)
        user = event_loop.run_until_complete(create_user(user_in_create))

    data = {'username': username, 'password': raw_password}
    response = client.post('/users/signin', data=data)
    auth_token = response.json()['access_token']
    return {'token_headers': {'Authorization': f'Bearer {auth_token}'}, 'user_id': user.id}


@pytest.fixture(scope="module")
def user_info(client: TestClient, event_loop: asyncio.AbstractEventLoop):
    return get_authentication_token(
        client=client, username='maintestuser2', event_loop=event_loop
    )

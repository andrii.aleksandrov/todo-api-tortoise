from fastapi import APIRouter, Depends
import fastapi_bearer_auth as fba

import app.models
from app import schemas
from app.crud import crud_user

router = APIRouter()


@fba.handle_get_user_by_name
async def get_user_by_name(name: str) -> app.models.User:
    return await crud_user.user.get_by_username(username=name)


@fba.handle_create_user
async def create_user(username: str, password: str) -> app.models.User:
    if await get_user_by_name(username):
        raise ValueError('Username {} exists'.format(username))
    hashed_password = await fba.call_config('get_password_hash', password)
    user_in = schemas.user.UserCreate(username=username, password=hashed_password)
    return await crud_user.user.create(user=user_in)


@router.post('/signup', response_model=schemas.user.User)
async def signup(user: schemas.user.UserCreate = Depends(fba.signup)):
    return await schemas.user.User.from_tortoise_orm(user)


@router.post('/signin')
async def signin(data=Depends(fba.signin)):
    return data['token']


@router.get('/me')
async def get_user(user=Depends(fba.get_current_user)):
    return user

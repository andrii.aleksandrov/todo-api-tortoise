from typing import List
from fastapi import APIRouter, Depends, HTTPException, status, Response
import fastapi_bearer_auth as fba

from app.crud import crud_item
from app.schemas import user as user_schemas
from app.schemas import item as item_schemas

router = APIRouter()


@router.get('/', response_model=List[item_schemas.Item])
async def get_items(current_user: user_schemas.User = Depends(fba.get_current_user)):
    items = await crud_item.item.get_collection(owner_id=current_user.id)
    return await item_schemas.Item.from_queryset(items)


@router.get('/{item_id}', response_model=item_schemas.Item)
async def get_item(item_id: int, current_user: user_schemas.User = Depends(fba.get_current_user)):
    item_db = await crud_item.item.get(item_id=item_id)
    item = await item_schemas.Item.from_tortoise_orm(item_db)
    if item_db.owner_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)
    return item


@router.post('/', response_model=item_schemas.Item, status_code=201)
async def create_item(item: item_schemas.ItemCreate, current_user: user_schemas.User = Depends(fba.get_current_user)):
    item_db = await crud_item.item.create(item=item, owner_id=current_user.id)
    return await item_schemas.Item.from_tortoise_orm(item_db)


@router.put('/{item_id}', response_model=item_schemas.Item)
async def update_item(item_id: int, item: item_schemas.ItemUpdate,
                      current_user: user_schemas.User = Depends(fba.get_current_user)):
    item_db = await crud_item.item.get(item_id=item_id)
    if not item_db:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    if item_db.owner_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)
    await crud_item.item.update(item_id=item_id, item_update=item)
    item_db = await crud_item.item.get(item_id=item_id)

    return await item_schemas.Item.from_tortoise_orm(item_db)


@router.delete('/{item_id}', status_code=204)
async def delete_item(item_id: int,
                      current_user: user_schemas.User = Depends(fba.get_current_user)):
    item_db = await crud_item.item.get(item_id=item_id)
    if not item_db:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    if item_db.owner_id != current_user.id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)
    await crud_item.item.delete(item_id=item_id)
    return Response()

from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise
import fastapi_bearer_auth as fba

from app.routers import items, users
from app import settings

app = FastAPI()

app.include_router(users.router, prefix='/users', tags=['users'])
app.include_router(items.router, prefix='/items', tags=['items'])

register_tortoise(
    app,
    db_url=f'postgres://{settings.SQL_USER}:{settings.SQL_PASSWORD}@{settings.SQL_HOST}/{settings.SQL_DATABASE}',
    modules={"models": ["app.models"]},
    add_exception_handlers=True,
)

fba.set_config({'SECRET_KEY': settings.SECRET_KEY, 'ALGORITHM': settings.ALGORITHM,
                'ACCESS_TOKEN_EXPIRE_MINUTES': settings.ACCESS_TOKEN_EXPIRE_MINUTES})

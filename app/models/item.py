from tortoise import fields, models


class Item(models.Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=50)
    description = fields.TextField()
    owner = fields.ForeignKeyField('models.User', related_name='users')

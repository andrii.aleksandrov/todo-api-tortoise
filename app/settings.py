import os

SECRET_KEY = 'KLDSKakI@!dkajSKDJdkjkJ@KkdjKJSAdj932ujdKLDJ'
ACCESS_TOKEN_EXPIRE_MINUTES = 60 * 60 * 3
ALGORITHM = 'HS256'

SQL_USER = os.environ.get('SQL_USER')
SQL_PASSWORD = os.environ.get('SQL_PASSWORD')
SQL_HOST = os.environ.get('SQL_HOST')
SQL_DATABASE = os.environ.get('SQL_DATABASE')


TORTOISE_ORM = {
    'connections': {'default': f'postgres://{SQL_USER}:{SQL_PASSWORD}@{SQL_HOST}/{SQL_DATABASE}'},
    'apps': {
        'models': {
            'models': ['app.models', 'aerich.models'],
            'default_connection': 'default',
        },
    },
}
from app.models.user import User
from app.schemas import user as user_schemas


class CRUDUser:
    async def create(self, user: user_schemas.UserCreate) -> User:
        return await User.create(**user.dict(exclude_unset=True))

    async def get(self, id: int):
        return await User.get_or_none(id=id)

    async def get_by_username(self, username: str):
        return await User.get_or_none(username=username)


user = CRUDUser()

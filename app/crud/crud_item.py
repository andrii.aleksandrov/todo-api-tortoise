from app.models.item import Item
from app.schemas import item as item_schemas


class CRUDItem:
    async def create(self, owner_id: int, item: item_schemas.ItemCreate):
        return await Item.create(**item.dict(exclude_unset=True), owner_id=owner_id)

    async def get(self, item_id: int):
        item = await Item.get(id=item_id)
        return item
    
    async def get_collection(self, owner_id):
        return Item.filter(owner_id=owner_id)

    async def update(self, item_id: int, item_update: item_schemas.ItemUpdate):
        return await Item.filter(id=item_id).update(**item_update.dict(exclude_unset=True))

    async def delete(self, item_id):
        await Item.filter(id=item_id).delete()


item = CRUDItem()
